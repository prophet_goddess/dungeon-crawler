uniform ivec2 resolution;
uniform vec2 camera_position;
uniform vec2 camera_direction;
uniform int map_size;
uniform Image tiles;
uniform Image texture0;
uniform Image texture1;
uniform Image texture2;
uniform Image texture3;

vec3 floor_color = vec3(0);
vec3 ceiling_color = vec3(0);

vec3 getColorForPosition(vec3 ray_origin, vec3 ray_direction, vec3 vos, vec3 pos, vec3 nor, int texture_index) {
	vec3 col = ray_direction.y > 0. ? ceiling_color : floor_color;
    if( pos.y <= 1. && pos.y >= 0. ) {
        vec2 mpos = vec2(dot(vec3(-nor.z, 0.0, nor.x), pos), pos.y);
		if (texture_index == 0) {
        	col = texture2D(texture0, mpos).xyz;
        } else if (texture_index == 1) {
         	col = texture2D(texture1, mpos).xyz;
        }
    }
    return col;
}

// return texture index of wall or -1 if no wall detected
int wall_collision(vec2 coords) {
	vec4 tile_value = texture2D(tiles, coords * (1. / float(map_size))); // convert to texture space
	if (tile_value.r == 1.) {
		return 0;
	} else if (tile_value.g == 1.) {
		return 1;
	} else {
		return -1;
	}
}

int MAXSTEPS = 30;
vec3 castRay(vec3 ray_origin, vec3 ray_direction) {
	vec3 pos = floor(ray_origin);
	vec3 ri = 1.0/ray_direction;
	vec3 rs = sign(ray_direction);
	vec3 dis = (pos-ray_origin + 0.5 + rs*0.5) * ri;

	float res = 0.0;
	vec3 mm = vec3(0.0);

	for( int i=0; i<MAXSTEPS; i++ )	{
		mm = step(dis.xyz, dis.zyx);
		dis += mm * rs * ri;
        pos += mm * rs;

        int texture_index = wall_collision(pos.xz);
		if( texture_index != -1 ) {
			vec3 mini = (pos-ray_origin + 0.5 - 0.5*vec3(rs))*ri;
			float t = max ( mini.x, mini.z );
			return getColorForPosition(ray_origin, ray_direction, pos, ray_origin+ray_direction*t, -mm*sign(ray_direction), texture_index);
		}
	}

	return ray_direction.y>0.?vec3(floor_color):vec3(ceiling_color);
}

vec4 effect(vec4 color, Image texture, vec2 texcoord, vec2 pixel_coords) {
	vec2 q = pixel_coords.xy / resolution.xy;
    vec2 p = -1.0 + 2.0 * q;
    p.x *= resolution.x / resolution.y;

	vec3 ray_origin = vec3(camera_position.x, 0.5, camera_position.y);
	vec3 camera_direction_with_y = vec3(camera_direction.x, 0, camera_direction.y);

	vec3 uu = normalize(cross(vec3(0., 1., 0.), camera_direction_with_y));
    vec3 vv = normalize(cross(camera_direction_with_y, uu));
    vec3 ray_direction = normalize( p.x * uu + p.y * vv + 2.5 * camera_direction_with_y );

	vec3 col = castRay(ray_origin, ray_direction);

	return vec4(col, 1.0);
}
