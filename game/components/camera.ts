import { Component } from "encompass-ecs";
import { Vector } from "game/helpers/vector";

export class CameraComponent extends Component {
    public plane: Vector;
}