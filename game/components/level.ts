import { Component } from "encompass-ecs";
import { GCOptimizedList } from "tstl-gc-optimized-collections";
import { Tile } from "game/helpers/tile";

export class LevelComponent extends Component {
    public size: number;
    public tile_image: Image;
    public map: GCOptimizedList<GCOptimizedList<Tile>>;
}
