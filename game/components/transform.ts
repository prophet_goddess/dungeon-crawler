import { Component } from "encompass-ecs";
import { Vector } from "game/helpers/vector";

export class TransformComponent extends Component {
    public position: Vector;
    public direction: Vector;
}