import { GCOptimizedSet, GCOptimizedList, GCOptimizedMap } from "tstl-gc-optimized-collections";
import { Engine, Message, Type, Reads, Emits } from "encompass-ecs";
import { GenerateLevelMessage } from "game/messages/generate_level";
import { SpawnPlayerMessage } from "game/messages/spawn_player";
import { LevelComponent } from "game/components/level";
import { Tile } from "game/helpers/tile";
import { Vector } from "game/helpers/vector";

interface Room {
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    center_x: number,
    center_y: number
}

@Reads(GenerateLevelMessage)
@Emits(SpawnPlayerMessage)
export class GenerateLevelEngine extends Engine {

    public update() {

        const generate_level_messages = this.read_messages(GenerateLevelMessage);
        for (const generate_level_message of generate_level_messages.iterable()) {

            //destroy existing level if it exists
            const existing_levels = this.read_components(LevelComponent);
            for (const existing_level of existing_levels.iterable()) {
                const level_entity = this.get_entity(existing_level.entity_id);
                if (level_entity != null) {
                    level_entity.destroy();
                }
            }

            //set up the map

            const size = generate_level_message.size;
            const level_entity = this.create_entity();
            const level_component = level_entity.add_component(LevelComponent);
            level_component.size = size;

            const map = level_component.map = new GCOptimizedList<GCOptimizedList<Tile>>();

            for (let x = 0; x < size; x++) {
                map.add(new GCOptimizedList<Tile>());
                for (let y = 0; y < size; y++) {
                    const tile = new Tile(false, 0);
                    let row = map.get(x);
                    if (row != null) {
                        tile.position = new Vector(x, y);
                        row.add(tile);
                    }
                }
            }

            const num_rooms = love.math.random(8, 16);
            const min_room_size = love.math.random(6, 12);
            const max_room_size = love.math.random(8, 16);
            let rooms = new GCOptimizedList<Room>();
            for (let i = 0; i < num_rooms; i++) {
                const x = love.math.random(1, size - (min_room_size) - 3);
                const y = love.math.random(1, size - (min_room_size) - 3);
                const w = love.math.random(min_room_size, Math.min(size - x - 1, max_room_size));
                const h = love.math.random(min_room_size, Math.min(size - y - 1, max_room_size))

                let new_room: Room = {
                    x1: x,
                    y1: y,
                    x2: x + w,
                    y2: y + h,
                    center_x: 0,
                    center_y: 0

                }

                new_room.center_x = Math.round((new_room.x1 + new_room.x2) / 2);
                new_room.center_y = Math.round((new_room.y1 + new_room.y2) / 2);


                this.draw_room(map, new_room);

                let prev_room = rooms.get(rooms.size - 1);
                if (prev_room != null) {
                    if (love.math.random() <= 0.5) {
                        this.draw_horiz(map, prev_room, new_room, prev_room.center_y);
                        this.draw_vert(map, prev_room, new_room, new_room.center_x);
                    }
                    else {
                        this.draw_vert(map, prev_room, new_room, prev_room.center_x);
                        this.draw_horiz(map, prev_room, new_room, new_room.center_y);
                    }
                }

                rooms.add(new_room);
            }

            const image_data = love.image.newImageData(size, size);
            for (const [x, row] of map.iterable()) {
                for (const [y, tile] of row.iterable()) {
                    if (!tile.walkable) {
                        image_data.setPixel(x - 1, y - 1, 1, 0, 0, 1);
                    } else {
                        image_data.setPixel(x - 1, y - 1, 0, 0, 0, 0);
                    }
                }
            }

            level_component.tile_image = love.graphics.newImage(image_data);

            image_data.encode("png", "level_data.png");

            let start_room = rooms.get(0);

            if (start_room != null) {
                let spawn_player = this.emit_message(SpawnPlayerMessage);
                spawn_player.x = start_room.center_x;
                spawn_player.y = start_room.center_y;
            }
        }

    }

    private draw_room(map: GCOptimizedList<GCOptimizedList<Tile>>, room: Room) {
        for (let x = room.x1; x < room.x2; x++) {
            for (let y = room.y1; y < room.y2; y++) {
                let row = map.get(x);
                if (row != null) {
                    let tile = row.get(y);
                    if (tile != null) {
                        tile.walkable = true;
                    }
                }
            }
        }
    }

    private draw_horiz(map: GCOptimizedList<GCOptimizedList<Tile>>, a: Room, b: Room, y: number) {
        for (let x = Math.min(a.center_x, b.center_x); x <= Math.max(a.center_x, b.center_x); x++) {
            let row = map.get(x);
            if (row != null) {
                let tile = row.get(y);
                if (tile != null) {
                    tile.walkable = true;
                }
            }
        }
    }

    private draw_vert(map: GCOptimizedList<GCOptimizedList<Tile>>, a: Room, b: Room, x: number) {
        let row = map.get(x);
        for (let y = Math.min(a.center_y, b.center_y); y <= Math.max(a.center_y, b.center_y); y++) {
            if (row != null) {
                let tile = row.get(y);
                if (tile != null) {
                    tile.walkable = true;
                }
            }
        }
    }
}
