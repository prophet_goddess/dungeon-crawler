import { Engine, Reads, Type, Message } from "encompass-ecs";
import { SpawnPlayerMessage } from "game/messages/spawn_player";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";
import { CameraComponent } from "game/components/camera";
import { TransformComponent } from "game/components/transform";
import { Vector } from "game/helpers/vector";

@Reads(SpawnPlayerMessage)
export class SpawnPlayerEngine extends Engine {
    public update() {
        const spawn_player_messages = this.read_messages(SpawnPlayerMessage);
        for (const spawn_player_message of spawn_player_messages.iterable()) {
            const player_entity = this.create_entity();
            const camera = player_entity.add_component(CameraComponent);
            const transform = player_entity.add_component(TransformComponent);

            camera.plane = new Vector(0.0, 0.66);
            transform.position = new Vector(spawn_player_message.x, spawn_player_message.y);
            transform.direction = new Vector(-1, 0);

        }
    }
}