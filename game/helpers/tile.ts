import { Component } from "encompass-ecs";
import { Vector } from "./vector";

export class Tile {
    public walkable: boolean;
    public texture_index: number;
    public position: Vector;

    public constructor(walkable: boolean, texture_index: number) {
        this.walkable = walkable;
        this.texture_index = texture_index;
    }
}
