export class Vector {

    public x: number;
    public y: number;

    public equals(other: Vector) {
        return other.x == this.x && other.y == this.y;
    }

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}