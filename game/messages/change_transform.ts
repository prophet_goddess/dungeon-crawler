import { ComponentMessage, Message } from "encompass-ecs";
import { TransformComponent } from "game/components/transform";
import { Vector } from "game/helpers/vector";

export class ChangeTransformMessage extends Message implements ComponentMessage {
    public component: TransformComponent;
    public new_position: Vector;
    public new_direction: Vector;
}
