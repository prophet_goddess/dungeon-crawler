import { GeneralRenderer } from "encompass-ecs";
import { CameraComponent } from "game/components/camera";
import { LevelComponent } from "game/components/level";
import { TransformComponent } from "game/components/transform";
import { GCOptimizedMap, GCOptimizedSet, GCOptimizedList } from "tstl-gc-optimized-collections";
import { Tile } from "game/helpers/tile";

export class FirstPersonRenderer extends GeneralRenderer {
    public layer = 0;

    private wall_shader: Shader;
    private tile_map = new GCOptimizedMap<number, Tile>();
    private buffer_canvas: Canvas;
    private width_scale: number = 1;
    private height_scale: number = 1;
    private width: number = 320;
    private height: number = 240;

    public initialize() {
        this.buffer_canvas = love.graphics.newCanvas(this.width, this.height);
        this.wall_shader = love.graphics.newShader("game/assets/shaders/wall.frag");
        this.wall_shader.send("resolution", [this.width, this.height]);
        const wall_texture = love.graphics.newImage("game/assets/textures/wall.png");
        wall_texture.setWrap("repeat", "repeat");
        this.wall_shader.send("texture0", wall_texture);
    }

    public render() {
        const canvas = love.graphics.getCanvas()!;

        const levels = this.read_components(LevelComponent);

        for (const level of levels.iterable()) {

            const cameras = this.read_components(CameraComponent);

            for (let x = 0; x < level.size; x++) {
                const column = level.map.get(x);
                if (column != null) {
                    for (let y = 0; y < level.size; y++) {
                        const tile = column.get(y);
                        if (tile != null) {
                            const existing = this.tile_map.get(level.size * tile.position.y + tile.position.x);
                            if (existing !== tile) {
                                this.tile_map.set(level.size * tile.position.y + tile.position.x, tile);
                            }
                        }
                    }
                }
            }

            for (const camera of cameras.iterable()) {
                const camera_entity = this.get_entity(camera.entity_id);
                if (camera_entity != null) {
                    const w = this.buffer_canvas.getWidth();
                    const h = this.buffer_canvas.getHeight();
                    const transform = camera_entity.get_component(TransformComponent);

                    this.wall_shader.send("camera_position", [transform.position.x, transform.position.y]);
                    this.wall_shader.send("camera_direction", [transform.direction.x, transform.direction.y]);
                    this.wall_shader.send("map_size", level.size);
                    this.wall_shader.send("tiles", level.tile_image);

                    love.graphics.setCanvas(this.buffer_canvas);
                    love.graphics.setBlendMode("alpha");
                    love.graphics.setColor(0, 0, 0, 1);
                    love.graphics.clear();
                    love.graphics.setShader(this.wall_shader);
                    love.graphics.rectangle("fill", 0, 0, w, h);
                    love.graphics.setShader();
                    love.graphics.setCanvas(canvas);
                }
            }
        }

        if (canvas !== null) {
            const pixel_width = Math.floor(this.width_scale * canvas.getWidth());
            const pixel_height = Math.floor(this.height_scale * canvas.getHeight());
            love.graphics.setColor(1, 1, 1, 1);
            love.graphics.setBlendMode("alpha", "premultiplied");
            love.graphics.draw(this.buffer_canvas, 0, 0, 0, pixel_width / this.width, pixel_height / this.height);
        }
    }
}
